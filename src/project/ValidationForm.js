import Input from "../input/Input";
import { useState } from "react";
import ValidationCheckButton from "./ValidationCheckButton";
import styled from 'styled-components';

export default function ValidationForm({
  placeholder,
  initialValue,
  onChange,
  onValidate,
  isValidated,
  isKnown
}) {
  const [value, setValue] = useState(initialValue || '');

  function handleChange(event) {
    setValue(event.target.value);
    if (onChange) onChange(event.target.value);
  }

  function handleSubmit(event) {
    event.preventDefault();
    onValidate(value);
  }

  return (
    <FormContainer onSubmit={handleSubmit}>
      <Input
        type='text'
        value={value}
        onChange={handleChange}
        placeholder={placeholder}
        className="project-number-input" 
      />
      <ValidationCheckButton
        type="submit"
        style={isValidated ? { backgroundColor: 'green', color: '#fff', display: !isKnown ? 'none' : 'inline' } : {} }
        disabled={isValidated}
        isChecked={isValidated} />
    </FormContainer >
  )
}

const FormContainer = styled.form`
  width: 100%;
  .project-number-input {
    width: 30%;
    outline-color: #79C4D4;
    &:hover {
      border-color: #94D0DD;
    }
  }
`
