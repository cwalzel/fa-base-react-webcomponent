import { useState } from "react";
// import { useTranslation } from "react-i18next";
import ValidationForm from "./ValidationForm";
import ContactData from "./ContactData";
import config from "../config.js"

export default function ProjectNumberValidation() {
  // const { t } = useTranslation();
  
  const [validated, setValidated] = useState(false)
  const [data, setData] = useState(false)
  const [projectNumber, setProjectNumber] = useState('')
  
  function fetchData(projectNumber) {
    const url = `${config.PROJECT_NUMBER_API_URL}?projectnr=${projectNumber}`;
    fetch(url, {
      "Content-Type": "application/json"
    }).then(
      response => response.json()
    )
    .then(
      json => setData(json)
    )
  }

  const getData = async () => fetchData(projectNumber)
    
  const onChange = (value) => {
    setProjectNumber(value)
    setData(false)
    setValidated(false)
  }
  
  const onValidate = (value) => {
    const valid = value.length === 12 && value.match(/[A-Z,\d]+/g)
    setValidated(valid)
    if (!valid) return setData({
      error: 'Die von Ihnen eingegebene Nummer ist keine FACTUREE-Projektnummer.',
      owner: {}
    })
    setProjectNumber(value)
    getData()
  }

  return (
    <>
        {/* <h3>{t('toolTitle')}</h3> */}
        <h3>Geben Sie bitte die Projektnummer ein, die Sie von uns erhalten haben.</h3>
        <ValidationForm
        placeholder='Projektnummer eintragen'
        initialValue={''}
        onChange={onChange}
        onValidate={onValidate}
        isValidated={validated} />
        {(data) && 
            <ContactData data={data} />
        }
    </>
  );
}