import { useState } from "react";
import styled from 'styled-components';
// import { useTranslation } from "react-i18next";
import {ReactComponent as IconPhone} from '../icons/icon-phone-facturee.svg'
import { ReactComponent as IconEmail } from '../icons/icon-email-facturee.svg'


export default function ContactData({
    data
}) {
//   const { t } = useTranslation();
  const [female] = useState(data.owner.female || '');
  const [name] = useState(data.owner.name || '');
  const [email] = useState(data.owner.email || '');
  const [phone] = useState(data.owner.phone || '');

  return (
    <DataContainer>
        {data.error ? null : <h4>Ihr Account Manager ist:</h4>}
        <Data>
            {data.error ?
                <span>
                    <div>
                        {data.error === 'noPrimaryOwnerFound' ? 'Diese Projektnummer konnte nicht gefunden werden' : data.error}
                    </div>
                </span>
            :
                <span>
                    <div>
                        <span>{female ? 'Frau ' : 'Herr '}</span>
                        {name}
                    </div>
                    <div>
                        <a
                            href={'mailto:' + email}>
                            <IconEmail
                                className="contact-icon"
                                alt="Email"
                                width="20"
                                height="20">
                            </IconEmail>
                            {email}
                        </a>
                    </div>
                    <div>
                        <a
                            href={'tel:' + phone}>
                            <IconPhone
                                className="contact-icon"
                                alt="Telefon"
                                width="20"
                                height="20">
                            </IconPhone>
                            {phone}
                        </a>
                    </div>
                </span>
            }
        </Data>
    </DataContainer >
  )
}

const DataContainer = styled.div`
    h4 {
        margin-bottom: .22rem;;
    }
`

const Data = styled.div`
    padding: 1rem;
    width: 100%;
    background-color: #EBF6F9;

    div:not(:first-child) {
        padding-top: .77rem;
    }

    .contact-icon  {
        transform: translateY(25%);
        margin-right: 0.77rem;
    }

    a {
        fill: #005B7F;
        color: #005B7F;
    }

    a:not([role=button]):not(.fa-button) {
        // color: #fd864f !important;
        text-decoration: none;
    }
    a:not([role=button]):not(.fa-button):hover {
        cursor: pointer;
        color: #fd864f;
        text-decoration: none;
        // border-bottom: 1px dotted #fd864f !important;
    }
    a:not([role=button]):not(.fa-button):active {
        cursor: pointer;
        text-decoration: none;
        // border-bottom: 1px dotted #fd864f;
    }

    // a:not([role=button]):visited,
    // a:not([role=button]).visited {
    //     color: gray;
    // }
    // a:not([role=button]):visited:hover,
    // a:not([role=button]).visited:hover {
    //     border-bottom: 1px dotted gray;
    // }
    // a:not([role=button]):visited:active,
    // a:not([role=button]).visited:active {
    //     border-bottom: 1px dotted gray;
    // }
`
