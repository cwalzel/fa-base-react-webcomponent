// import { useTranslation } from "react-i18next";
import styled from "styled-components";
import PrimaryButton from "../input/PrimaryButton";

export default function ValidationCheckButton({
  isChecked,
  ...props
}) {
  // const { t } = useTranslation();
  
  const checkButtonText = isChecked ?
    <i className="fa fa-check" /> :
    // t('submitButton');
    'Account Manager finden';

  return (
    <StyledValidationCheckButton
      disabled={isChecked}
      {...props}>
      {checkButtonText}
    </StyledValidationCheckButton>
  );
}

const StyledValidationCheckButton = styled(PrimaryButton)`
  padding: 0px;
  padding-left: 20px;
  padding-right: 20px;
`;
