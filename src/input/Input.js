import styled, { css } from "styled-components";

const Input = styled.input`
  font: inherit;
  background: none;
  color: inherit;

  ${props => props.type == null || ['text', 'email'].includes(props.type) ? css`
    // height: 44px;
    padding: 10px;
    border: 1px solid #aed6e0;
    border-radius: 3px;
  ` : null}
  
   ${props => props.type === 'checkbox' ? css`    
    ~ .label-right {
      padding-left: 18px;
      vertical-align: 2px;
    }
    
    &:disabled + span::before {
      opacity: .3;
    } 
  ` : null}

`;

export default Input;