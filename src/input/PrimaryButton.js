import styled from "styled-components";

const PrimaryButton = styled.button`
  color: #FFF;
  background-color: #fd864f;
  border: 1px solid #fd864f;
  border-radius: 3px;
  font-size: 1.1rem;
  font-weight: 600;
  padding: 7px max(18px, min(5%, 60px));
  height: 46px;
  max-height: 46px;
  line-height: initial;
  
  &:hover:not(:disabled) {
    cursor: pointer;
    fill: #fd864f;
    color: #fd864f;
    background-color: #FFF;
    border: 1px solid #ef783c;
    -webkit-transition: all 0.3s;
    -o-transition: all 0.3s;
    transition: all 0.3s;
    
    .f-icon-uni {
      background-color: #fd864f;
    }
  }

  &:disabled {
    cursor: not-allowed;
    fill: #b6cfda;
    color: #b6cfda;
    background-color: #eef4f6;
    border: 1px solid #eef4f6;
  } 
  
  &.w-280 {
    width: 280px;
  }
  
  &.w-360 {
    width: 360px;
  }
`;

export default PrimaryButton;