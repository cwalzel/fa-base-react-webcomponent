const config = {
    PROJECT_NUMBER_API_URL: process.env.REACT_APP_PROJECT_NUMBER_API_URL,
    ENVIRONMENT : process.env.REACT_APP_ENVIRONMENT,
};

export default config;
