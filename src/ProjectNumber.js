// import { I18nextProvider } from "react-i18next";
// import i18next from "i18next";
// import common_de from "./translations/de/common.json";
// import common_en from "./translations/en/common.json";
// import LanguageDetector from 'i18next-browser-languagedetector';
import ProjectNumberValidation from "./project/ProjectNumberValidation";

export default function ProjectNumber() {
  // i18next.use(LanguageDetector).init({
  //   interpolation: { escapeValue: false },  // React already does escaping
  //   defaultNS: 'common',
  //   resources: {
  //   //   en: {
  //   //     "common": common_en               // 'common' is our custom namespace
  //   //   },
  //     de: {
  //       "common": common_de
  //     },
  //   //   es: {
  //   //     "common": common_es
  //   //   },
  //   //   fr: {
  //   //     "common": common_fr
  //   //   },
  //   //   it: {
  //   //     "common": common_it
  //   //   },
  //   },
  //   detection: {
  //     order: ['querystring', 'cookie', 'localStorage', 'sessionStorage', 'navigator', 'htmlTag', 'path', 'subdomain'],
  //   }
  // });

  return (
    // <I18nextProvider i18n={i18next}>
        <ProjectNumberValidation />
    // </I18nextProvider>
  );
}