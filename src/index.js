import React from "react";
import ReactDOM from "react-dom";
import reactToWebComponent from "react-to-webcomponent";
// import App from "./App";
// import ProjectNumberValidation from "./project/ProjectNumberValidation";
import ProjectNumber from "./ProjectNumber";
import { name } from '../package.json'

const app = reactToWebComponent(ProjectNumber, React, ReactDOM);

const COMPONENT_NAME = `${ name }-webcomponent`

customElements.define(COMPONENT_NAME, app);
